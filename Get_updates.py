import requests
import datetime
import re
from bs4 import BeautifulSoup


# returns every entry found as a 2D matrix
def parse_data(data, target_author):
    results = []
    for hit in re.findall(f'{target_author}.+', data):
        hit = hit.split('|')
        results.append(
            [hit[i] for i in [4, 3, 1, -1, 2]])
    return results


# gets the raw date for today
def request_data():
    current_time = datetime.date.today()
    raw_data = requests.get(
        f"http://samlib.ru/logs/{current_time.year}/{current_time:%m}-{current_time:%d}.log")
    return raw_data


# creates html table with headers;
# excepts list of header names and soup where the new table is added
def create_and_inject_table(data_table, header_names, soup):
    new_table = soup.new_tag("table")
    for name in header_names:
        new_entry = soup.new_tag("th")
        new_entry.append(name)
        new_table.append(new_entry)
    soup.body.append(new_table)
    for row in data_table:
        new_row = soup.new_tag("tr")
        for col in row:
            new_col = soup.new_tag("td")
            new_col.append(col)
            new_row.append(new_col)
        soup.table.append(new_row)
    return 0


# template_file is the file that will not be changed
# it is there to form the soup, hence the name 'template'
# out_file is the file where table will be injected

default_template = "<html><head><link href='style.css' rel='stylesheet' /></head><body></body></html>"


def form_file(data_table, header_names, template_file=default_template):
    soup = BeautifulSoup(template_file, features='lxml')
    create_and_inject_table(data_table, header_names, soup)
    with open("output.html", "w", encoding="utf8") as out_file:
        out_file.write(str(soup.prettify()))
    return 0

if __name__ == '__main__':
    r = request_data()
    data_table = parse_data(r.text, "")
    header_names = ['Author', 'Title', 'Tag', 'Size', 'Stamp']
    form_file(data_table, header_names)

#TODO make header names depend on parse_data